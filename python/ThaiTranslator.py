# CS124 Homework 6 Machine Translation

import codecs
import sys
from copy import deepcopy
from subprocess import *
from CustomLanguageModel import CustomLanguageModel

customLM = 0

# Dictionary format:
# {"entry" : {"def":[DEFINITION HERE], "pos": [LIST OF POS TAGS HERE]}, etc..}

# Class encapsulating the dictionary
class ThaiTranslator:
    def __init__(self):
        self.dictionary = {}
        self.verb_tenses = {}
    
    def loadDictionary(self, file_name, enc):
        engDefs = set()
        with codecs.open(file_name, encoding=enc) as f:
            line_count = 0
            for line in f:
                line_count += 1
                if line_count == 1:
                    continue
                pair = line.strip().split(':')
                self.dictionary[pair[0]] = \
                    [{"def":s.split('_')[0], "pos":s.split('_')[1:]} \
                    for s in pair[1].split(',')]
                    
    def conjugateVerb(self, verb, number=1):
        if verb not in self.verb_tenses:
          if number == 1:
            if verb == "be":
              result = "is"
            elif verb[-1] in ['o', 'x'] or verb[-2:] in ['ss', 'sh', 'ch', 'zz']:
              result = "%ses" % verb
            elif verb[-1] == 'y':
              result = "%sies" % verb[:-1]
            else:
              result = "%ss" % verb
          else:
            if verb == "be":
              result = "are"
            else:
              result = verb
        else:
          result = self.verb_tenses[verb][number-1]
        #print "Modifying %d nouns before, '%s' -> '%s'" % \
        #      (number, verb, result)
        return result

# Loads the dev_E/F files, and put them into a list, one line per list entry
def loadList(file_name, enc):
    with codecs.open(file_name, encoding=enc) as f:
        l = [line.strip() for line in f]
    return l

# Sentence format during processing: [{"token":[ACTUAL STRING OF TOKEN],
# "pos":[POS Tag], "meaning":[{"def":[DEFINITION], "pos":[LIST OF POSSIBLE POS
# TAGS]}, ...] , ...]

# Debug: Given a sentence in the form of an array of dictionaries, print out
# the sentence
def printSentence(sentence):
  for i in xrange (0, len(sentence)):
    print sentence[i]["token"].encode('utf-8') + "_".encode('utf-8'),
    print sentence[i]["pos"].encode('utf-8'),
    '''
    print "Token: " + sentence[i]["token"].encode('utf-8')
    if "meaning" in sentence[i]:
      print "Meaning: " + str(sentence[i]["meaning"])
    if "pos" in sentence[i]:
      print "Pos: " + sentence[i]["pos"]
    print ""
    '''

# Debug: given an english word, return POS tags
def engPOSTags(tt, englishToken):
  possible = []

  if englishToken in ['her', 'his', 'mine', 'my', 'our', 'ours', \
                      'their', 'thy', 'your']:
    return ["PRP$"]

  for k, v in tt.dictionary.iteritems():
    for d in v:
      #if englishToken[-2:] == "'s":
      #  # Posessive
      #  return ["JJ"]

      if englishToken == d["def"]:
        return d["pos"]

      elif englishToken in d["def"]:
        possible = d["pos"]

  return possible

# Look up the dictionary for the array of english meanings, given a sentence
# (as defined on line 25)
def directTranslate(tt, sentence):
  for i in xrange (0, len(sentence)):
    sentence[i]["meaning"] = deepcopy(tt.dictionary[sentence[i]["token"]])

  return sentence

# Run the POS-Tagger on each token of the sentence (as defined on line 25)
def POSTag(sentence):
  tokenizerClassPaths = '-classpath "../opennlptools/opennlp-tools-1.4.3.jar:../opennlptools/ant.jar:../opennlptools/trove.jar:../opennlptools/maxent-2.5.2.jar"'
  tokenizerOtherArgs = '-Xmx100m opennlp.tools.lang.thai.PosTagger ../opennlptools/thai.tag.bin'
  for i in xrange (0, len(sentence)):
    runString = 'echo "%s" | java %s %s' % (sentence[i]["token"], tokenizerClassPaths, tokenizerOtherArgs)
    (output, err) = Popen(runString, stdout=PIPE, shell=True).communicate()
    rawPos = output[output.find ('/')+1:].strip()

    # Thai POS postprocessing - converting Thai tags to their closes english
    # tag. Actual meaning of thai tags can be found at:
    # Sornlertlamvanich, Virach, Thatsanee Charoenporn, and Hitoshi Isahara.
    # "ORCHID: thai part-of-speech tagged corpus." Orchid, TR-NECTEC-1997-001
    # (1997): 5-19.
    # Lazy link: http://www.researchgate.net/publication/2630580_Building_A_Thai_Part-Of-Speech_Tagged_Corpus_(ORCHID)/file/72e7e514db19a98619.pdf
    # Alternatively search scholar.google.com for the keywords: "ORCHID Thai"
    translatedPos = ""
    # Note there are only cases for POSs that appear in the corpus
    # Active/Stative/Attributive Verb
    if rawPos == "VACT" or rawPos == "VSTA" or rawPos == "VATT":
      translatedPos = "VB"
    # Preposition
    elif rawPos == "RPRE":
      translatedPos = "IN"
    # Indefinite determiner, following noun; allowing classifier in between
    elif rawPos == "DIAC":
      translatedPos = "DT"
    # coordinating conjuction
    elif rawPos == "JCRG":
      translatedPos = "CC"
    # common noun or title noun
    elif rawPos == "NCMN" or rawPos == "NTTL":
      translatedPos = "NN"
    # subordinating conjuction
    elif rawPos == "JSBR":
      translatedPos = "IN"
    # post-verb auxiliarry
    elif rawPos == "XVAE":
      translatedPos = "<UNK>"
    # adverb with normal form
    elif rawPos == "ADVN":
      translatedPos = "RB"
    # Personal pronoun or demonstrative pronoun
    elif rawPos == "PPRS" or rawPos == "PDMN":
      translatedPos = "PRP"
    # Determiner, cardinal number expression
    elif rawPos == "DCNM":
      translatedPos = "DT"
    # Pre-verb auxiliary, before negator "mai"
    elif rawPos == "XVBM":
      translatedPos = "RB"
    # Pre-verb, before or after negator "mai"
    elif rawPos == "XVMM":
      translatedPos = "RB"
    # The word "mai" - a negator
    elif rawPos == "NEG":
      translatedPos = "DT"
    else:
      sys.exit(0)

    sentence[i]["pos"] = translatedPos
  return sentence

# Given a raw list of characters, generate a data structure as described on
# line 25
def tokenize(tt, sentence):
  output = []
  currentCharacters = ""
  for i in xrange(0, len(sentence)):
    currentCharacters += sentence[i]
    # Simply match the shortest possible string
    if currentCharacters in tt.dictionary:
      output.append ({"token":currentCharacters})
      currentCharacters = ""
  return output

# Given a list of english sentences, return the one with the highest 4-gram
# probability
def selectBestSentenceUsingLanguageModel(sentences):
  global customLM

  if len(sentences) == 0:
    return ""

  best_score = -sys.maxint
  for s in sentences:
    score = customLM.score(s)
    if score > best_score:
      best_score = score
      best_sentence = s
  return best_sentence

# Returns all possible combinations of every meaning of a sentence
def constructSentences (sentence):
  possibleSentences = []
  soFar = []
  constructSentence (possibleSentences, soFar, sentence)
  return [s.strip() for s in possibleSentences]

def constructSentence (possibleSentences, soFar, sentence):
  if len(sentence) == 0:
    s = correctEngSentenceStructure(deepcopy(list(soFar)))
    soFarStr = ""
    for e in s:
        soFarStr += " " + e["def"]
    possibleSentences.append (soFarStr)
    return
  for i in xrange (0, len(sentence[0]["meaning"])):
    constructSentence (possibleSentences, soFar+[sentence[0]["meaning"][i]], sentence[1:])

# Try to intelligently select the correct meaning of each word by matching POS
# of the Thai and English words
def selectMeaning (tt, sentence):
  for i in xrange (0, len(sentence)):
    possibleMeanings = sentence[i]["meaning"]
    # If there's only one meaning - nothing to do
    if len(possibleMeanings) == 1:
      continue
    thaiPos = sentence[i]["pos"]
    first = True
    for j in xrange (0, len(possibleMeanings)):
      engPOSes = possibleMeanings[j]["pos"]
      # If the thai part of speech is a substring of the english part of speech
      if any(thaiPos in engPOS for engPOS in engPOSes):
        if first:
            sentence[i]["meaning"] = [possibleMeanings[j]]
            first = False
        else:
            sentence[i]["meaning"].append(possibleMeanings[j])
  return sentence

def swap(structure, a, b):
  temp = structure[a]
  structure[a] = structure[b]
  structure[b] = temp

def conjugateVerbs(tt, sentence_str):

  sentence = sentence_str.split()

  # Get tags
  pos_tags = []
  for i, x in enumerate(sentence):
    pos_tags.append(engPOSTags(tt, x))

  #print "Sentence: ", sentence
  #print "Tags: ", pos_tags

  # Find noun phrases and better order adjectives / pronouns
  start = 0
  end = 0
  i = 0
  noun_phrases = []
  assert len(sentence) == len(pos_tags)
  while i < len(sentence):
    has_phrase = True
    # Check if word is strictly an adjective, pronoun or noun
    for t in pos_tags[i]:
      if t[0] not in ['N', 'P', 'J'] and t != "CD":
        has_phrase = False
        break
    if has_phrase:
      end += 1
    else:
      if end > start:
        noun_phrases.append((start, end))
      start = i+1
      end = i+1
    i += 1

  # Add last noun phrase if it exists
  if end > start:
    noun_phrases.append((start, end))

  verb_locs = []
  #print "noun_phrases: %s" % noun_phrases
  offset = 0
  for start, stop in noun_phrases:
    start += offset
    stop += offset

    #print "noun phrase: %s" % sentence[start:stop]
    #print "pos_tags: %s" % pos_tags[start:stop]

    noun_loc = None
    found_determiner = False
    for i in range(start, stop):
      if [t for t in pos_tags[i] if t[0] == "N"]:
        noun_loc = i
        break

    # Check for pronouns and use as primary noun if found
    for i in range(start, stop):
      if [t for t in pos_tags[i] if "PRP" in t]:
        if not noun_loc:
          noun_loc = i
        found_determiner = True

    if noun_loc is not None:
      if [t for t in pos_tags[noun_loc] if t in "PRP"]:
        if "'s" in sentence[noun_loc]:
          found_determiner = True
        #sentence[noun_loc] = sentence[noun_loc].replace("'s","")

      if not found_determiner:
        sentence.insert(start, "the")
        pos_tags.insert(start, ["DT"])
        offset += 1

  # Find verbs that follow nouns
  noun_found = False
  conjunction = False
  num_phrases = 0
  for i, x in enumerate(sentence):
    #  # We have a infinitive or modal auxiliary so don't conjugate
    if [t for t in pos_tags[i] if t[0] == 'V']:
      # Conjugate if previous word is not a modal
      if noun_found and not [t for t in pos_tags[-2] if t in "MD"]:
        verb_locs.append((i, num_phrases))
      num_phrases = 0
      conjunction = False
      noun_found = False

    # Chedk for a noun
    elif [t for t in pos_tags[i] if t[0] == 'N' or t in 'PRP']:
      noun_found = True
      if sentence[i] in ["they", "we", "you", "I"]:
        num = 2
      else:
        num = 1

      if num_phrases < 1:
        # First noun found
        num_phrases += num
      elif num_phrases > 0 and conjunction:
        # Another found followed by a conjunction
        num_phrases += num

    elif [t for t in pos_tags[i] if t in ["CC"]]:
      # Restart phrase
      conjunction = True

  for loc, num in verb_locs:
    if "MD" not in pos_tags[loc-1]:
      sentence[loc] = tt.conjugateVerb(sentence[loc], number=verb_locs[0][1])

  #print "verb loc: ", verb_locs
  #print "After PP Sentence: ", sentence
  #print "After PP Tags: ", pos_tags

  sentence[0] = sentence[0].title()
  return " ".join(sentence) + '.'

# Hand written rules to correct the sentence structure based on POS tags of the
# Thai words
def correctSentenceStructure (sentence):
  # Remove <UNK> word
  for i in xrange(0, len(sentence)):
    if i > len(sentence)-1:
        break
    if sentence[i]["pos"] == "<UNK>":
        sentence.pop(i)
            
  return sentence

# Hand written rules to correct the sentence structure based on POS tags of the
# English words
def correctEngSentenceStructure (sentence):
    # Detect [OBJECT] OF [OWNER] by detecting NN of NN/PRP
    # Fix: NN of NN/PRP -> NN/PRP's NN
    for i in xrange(0, len(sentence)-2):
        if "NN" in sentence[i]["pos"] and sentence[i+1]["def"] == "of" and \
            any(x in sentence[i+2]["pos"] for x in ["NN", "PRP"]):
            nn = sentence[i]
            prp = sentence[i+2]
            if prp["def"] == "I":
                prp["def"] = "my"
                prp["pos"] = "PRP$"
            elif prp["def"] == "you":
                prp["def"] = "your"
                prp["pos"] = "PRP$"
            elif prp["def"] == "we":
                prp["def"] = "our"
                prp["pos"] = "PRP$"
            elif prp["def"] == "he":
                prp["def"] = "his"
                prp["pos"] = "PRP$"
            elif prp["def"] == "her":
                prp["def"] = "hers"
                prp["pos"] = "PRP$"
            elif prp["def"] == "they":
                prp["def"] = "their"
                prp["pos"] = "PRP$"
            else:
                prp["def"] += "'s"
            sentence[i] = prp
            sentence[i+2] = nn
            sentence.pop(i+1)
            
    # Detect PLURAL by detecting DT NN
    # Conjugate plural form by adding 's' (as of now)
    for i in xrange(0, len(sentence)-1):
        if "CD" in sentence[i]["pos"] and "NN" in sentence[i+1]["pos"]:
            sentence[i+1]["def"] += "s"
            
    # [ADJ] and [NOUN] swapping
    for i in xrange(0, len(sentence)-1):
        if any(x in sentence[i+1]["pos"] for x in ["JJ", "JJR", "JJS"]) and \
            "NN" in sentence[i]["pos"]:
            tmp = sentence[i]
            sentence[i] = sentence[i+1]
            sentence[i+1] = tmp
            
    return sentence

# Main processing function, call it with the ThaiTranslator dictionary
# encapsulating class and a sentence (an array of characters)
def processSentence(tt, sentence):
  print "Thai:" + sentence.encode('utf-8')

  # First, tokenize PRE
  sentence = tokenize(tt, sentence)

  # Direct translation
  sentence = directTranslate (tt, sentence)

  # Pos tag THAI (commented out since it takes a long time) PRE
  sentence = POSTag (sentence)

  # correct sentence structure using hand written rules POST/PRE
  sentence = correctSentenceStructure (sentence)
  
  # select the meaning of each word by using POS tags POST
  sentence = selectMeaning (tt, sentence)

  # After we've removed meanings by matching POS tags, we use brute force:
  # construct every possible english sentence by using every possible meaning
  # POST
  candidateSentences = constructSentences (sentence)

  #print candidateSentences
  # Run all candidate sentences through the language model to select the best
  # sentence
  bestSentence = selectBestSentenceUsingLanguageModel(candidateSentences)
  bestSentence = conjugateVerbs(tt, bestSentence)
  print "English:" + bestSentence
  '''
  print "POS:",
  for x in bestSentence.split(' '):
    print x,
    for y in engPOSTags(tt, x):
      print "_" + y,
  print ""
  '''

  #DBG
  #printSentence(sentence)

  '''
  print ""
  print ""
  '''

def main():
    # Load files
    dev_E_file = "../data/dev_E.txt"
    dev_F_file = "../data/dev_F.txt"
    test_E_file = "../data/test_E.txt"
    test_F_file = "../data/test_F.txt"
    
    dict_file = "../data/dict.txt"
    
    # Load dictionary into the encapsulating class
    tt = ThaiTranslator()

    # Explanation for [1:] : For some reason, a character is inserted before
    # the first character of a file, this makes it impossible to match the
    # first Thai word in a file with anything from the dictionary since there
    # is this 'special' character.
    # NOTE: for some reason, this 'special' character doesn't exist in the
    # dictionary file, which is why there is no padding line in ../data/dict.txt
    dev_E = loadList(dev_E_file, 'utf-8')
    dev_F = loadList(dev_F_file, 'utf-8')[1:]
    test_E = loadList(test_E_file, 'utf-8')
    test_F = loadList(test_F_file, 'utf-8')[1:]
    
    tt.loadDictionary(dict_file, 'utf-8')


    global customLM
    customLM = CustomLanguageModel()

    # XXX
    # Switch between DEV/TEST sets here
    '''
    for i in xrange(0, len(test_F)):
      processSentence(tt, test_F[i])
    '''
    for i in xrange(0, len(dev_F)):
      processSentence(tt, dev_F[i])

if __name__ == '__main__':
    main()
