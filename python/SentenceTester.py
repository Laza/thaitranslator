import sys
from CustomLanguageModel import CustomLanguageModel

def main():

    print 'Custom Language Model: '

    sentences = \
        ["I love translating Thai using machines because it is hard",
         "translate I Thai love with machines because hard is it", 
         "I enjoy change over to Thai words with machines while not easy"]

    customLM = CustomLanguageModel(max_ngrams=4)

    best_score = -sys.maxint
    for s in sentences:
        print "Checking: %s" % s
        score = customLM.score(s)
        print "score=%.2f" % score
        if score > best_score:
            best_score = score
            best_sentence = s
    print "Best: %s" % best_sentence

if __name__ == "__main__":
    main()
