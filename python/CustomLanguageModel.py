import math
from collections import defaultdict

class CustomLanguageModel:

  def __init__(self, corpus=None, max_ngrams=2, use_stop_words=False):
    """Initialize your data structures in the constructor."""

    self.max_ngrams = max_ngrams
    self.ngrams = defaultdict(lambda: defaultdict(lambda: 0))

    # Keep track of set of all types that follow given word
    self.followCounts = defaultdict(lambda: set([]))

    # Keep track of set of all types that precede given word
    self.completeCounts = defaultdict(lambda: set([]))
    self.total = 0
    self.kn_used = 0
    self.backoff_used = 0

    count_1w_path = open("./data/count_1w.txt")
    for line in count_1w_path:
        line = line.lower()
        word, count = line.split()
        self.ngrams[1][word] = int(count)

    count_1w_path = open("./data/count_2w.txt")
    for line in count_1w_path:
        line = line.lower()
        words = line.split()[0:2]
        count = int(line.split()[2])
        self.ngrams[2][" ".join(words)] = count
        prev, curr = words
        self.followCounts[prev].add(curr)
        self.completeCounts[curr].add(prev)

    #if not corpus:
    #    trainPath = './data/holbrook-tagged-train.dat'
    #    #trainingCorpus = HolbrookCorpus(trainPath, use_stop_words)
    #    trainingCorpus = HolbrookCorpus(trainPath)

    #    #devPath = './data/holbrook-tagged-dev.dat'
    #    #devCorpus = HolbrookCorpus(devPath)
    #self.train(trainingCorpus)

  def train(self, corpus):
    """ Takes a corpus and trains your language model. 
        Compute any counts or other corpus statistics in this function.
    """  
    for sentence in corpus.corpus:
      #print "sentence = %s" % sentence
      for i in range(len(sentence.data)): 
        for n in range(1, self.max_ngrams+1):
            if i+1 >= n:
                token = " ".join(w.word for w in sentence.data[i-n+1:i+1])
                #print "tokenizing %d: %s" % (n, token)
                self.ngrams[n][token] += 1

        if i > 0:
            prev_token = sentence.data[i-1].word
            curr_token = sentence.data[i].word

            # Keep track of set of all types that follow previous word
            self.followCounts[prev_token].add(curr_token)

            # Keep track of set of all types that finish with given word
            self.completeCounts[curr_token].add(prev_token)

        self.total += 1

    print "Built LM with %d" % len(corpus.corpus)
    import operator
    for n in range(1, self.max_ngrams+1):
        sorted_dict = sorted(self.ngrams[n].iteritems(), 
                             key=operator.itemgetter(1), reverse=True)
        print "Top 10 %d-grams: " % n
        for key, value in sorted_dict[:10]:
            print "%s: %s" % (key, value)
        print


    # Display top follow and complete counts
    sorted_dict = sorted(self.followCounts.iteritems(), 
                         key=lambda x: len(x[1]), reverse=True)
    print "followCounts:"
    for key, value in sorted_dict[:10]:
        print "%s: %s" % (key, len(value))

    sorted_dict = sorted(self.completeCounts.iteritems(), 
                         key=lambda x: len(x[1]), reverse=True)
    print "completeCounts:"
    for key, value in sorted_dict[:10]:
        print "%s: %s" % (key, len(value))

  def get_backoff(self, tokens):
    tokens_str = " ".join(tokens)
    score = 0.0
    K = 1
    V = len(self.ngrams[1])
    D = 0.4
    if tokens_str in self.ngrams[2]:
      # Try with bigram
      count = self.ngrams[2][tokens_str]
      prev_total = self.ngrams[1][tokens[0]]
      score += math.log(count)
      score -= math.log(prev_total)
    else:
      # Backoff to add-one unigram
      count = self.ngrams[1][tokens[1]]
      score += math.log(D)
      score += math.log(count + K)
      score -= math.log(self.total + V)
    return score

  def score(self, sentence):
    """ Takes a list of strings as argument and returns the log-probability of 
        the sentence using your language model. 
    """
    score = 0.0 
    D = 0.4

    for i in range(1, len(sentence)):
      tokens_str = " ".join(sentence[i-1:i+1])
      #print "tokens_str = %s" % tokens_str
      if sentence[i-1] in self.ngrams[1] and \
          sentence[i] in self.ngrams[1] and \
          tokens_str in self.ngrams[2]:

        prev_total = self.ngrams[1][sentence[i-1]]
        count = self.ngrams[2][tokens_str]
        kn_lhs = max(count - D, 0) / prev_total
        
        prev_types = len(self.followCounts[sentence[i-1]])
        lambda_wprev = D / prev_total * prev_types

        p_continuation = float(len(self.completeCounts[sentence[i]])) \
                              / len(self.ngrams[2])
        kn_rhs = lambda_wprev * p_continuation
        kn_score = math.log(kn_lhs + kn_rhs)
        #print "Kneser-Ney score: %.3f" % kn_score
        self.kn_used += 1
        score += kn_score
      else:
        backoff_score = self.get_backoff(sentence[i-1:i+1])
        #print "backoff score: %s" % backoff_score
        self.backoff_used += 1
        score += backoff_score

    return score
